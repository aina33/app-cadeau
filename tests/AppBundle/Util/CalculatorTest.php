<?php
/**
 * Created by PhpStorm.
 * User: herizo
 * Date: 11/6/18
 * Time: 3:07 PM
 */

namespace Tests\App\Util;



use App\Util\Calculator;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class CalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new Calculator();
        $result = $calculator->add(5, 6);

        $this->assertEquals(11, $result);
    }
}